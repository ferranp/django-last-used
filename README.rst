================
django-last-used
================

Keep track of last used objects in a django app.

Installation
============

#. Install with ``pip install django-last-used"`` or ddd ``"last_used"`` directory to your Python path.
#. Add ``"last_used"`` to the ``INSTALLED_APPS`` tuple found in
   your settings file.
#. Run ``manage.py syncdb`` to create the new tables

Usage
=====

For save used objects::

    import last_used

    # save used object
    last_used.use(object=item, user=user)

    # save with a key
    last_used.use(object=article, user=user, key="read")

    last_used.use(object=article, user=user, key="seen")


For get last used objects::

    import last_used

    # get last used
    last_used_articles = last_used.get(model=Article, user=user)

    # get with a key
    last_read_articles = last_used.get(model=Article, user=user, key="read")


