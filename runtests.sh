#!/bin/sh

MODULES=${1:-last_used}

coverage run --branch ./manage.py test $MODULES || exit 2
coverage report --include="./last_used/*" --omit="./*/migrations/*"
coverage html --include="./last_used/*" --omit="./*/migrations/*"
